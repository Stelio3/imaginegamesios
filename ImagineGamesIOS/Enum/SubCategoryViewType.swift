//
//  TypeOfGame.swift
//  ImagineGamesIOS
//
//  Created by Alberto Gurpegui Ramon on 11/02/2019.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

enum SubCategoryViewType {
    case tablegame
    case videogame
}

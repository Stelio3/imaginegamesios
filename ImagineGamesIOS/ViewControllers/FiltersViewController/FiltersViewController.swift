//
//  FiltersViewController.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 17/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class FiltersViewController: UIViewController {
    
    let MyCollectionViewCellId: String = "MyCollectionViewCell"
    
    @IBOutlet weak var collectionView: UICollectionView?
    
    
    private var repositoryFilters:LocalFilterRepository?
    private var filters: [Filter] = []
    private var dataType: SubCategoryViewType?
    
    init(){
        super.init(nibName: "FiltersViewController", bundle: nil)
        //self.tabBarItem.image = UIImage(named: "series")

        
        self.title = NSLocalizedString("titled_filters", comment: "")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        repositoryFilters = LocalFilterRepository()
        if let allFilters = repositoryFilters?.getAll(){
            filters = allFilters
        }
    }
    
    @IBAction func cleanFilters(_ sender: Any) {
        collectionView?.reloadData()
        print("Filters clean")
    }
    
    func registerCell() {
        let identifier = "FiltersCell"
        let cellNib2 = UINib(nibName: identifier, bundle: nil)
        collectionView?.register(cellNib2, forCellWithReuseIdentifier: identifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}


extension FiltersViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filters.count - 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset = 60
        return UIEdgeInsetsMake(CGFloat(inset), CGFloat(inset), CGFloat(inset), CGFloat(inset))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        let filter = filters[indexPath.row]
        if cell?.layer.backgroundColor == UIColor(red: 1, green: 0.91, blue: 0.54, alpha: 1.0).cgColor {
            cell?.layer.backgroundColor = UIColor.lightGray.cgColor
            print("Diselected filter: " + filter.title)
        }else {
            cell?.layer.backgroundColor = UIColor(red: 1, green: 0.91, blue: 0.54, alpha: 1.0).cgColor
            print("Selected filter: " + filter.title)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 100, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: FiltersCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FiltersCell", for: indexPath) as! FiltersCell
        let filter = filters[indexPath.row]
        cell.imgFilter?.image = UIImage(named: filter.image)
        return cell
    }
   
}

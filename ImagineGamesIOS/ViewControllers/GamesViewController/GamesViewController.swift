//
//  TableGamesViewController.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 17/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

enum GamesViewType {
    case tablegame
    case videogame
}

class GamesViewController: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    private var repositoryTableGamesCategory:LocalSubCategoryRepository?
    private var repositoryVideoGamesCategory:LocalSubCategoryRepository?
    private var repositoryTableGames:LocalTableGamesRepository?
    private var repositoryVideoGames:LocalVideoGamesRepository?
    private var subCategoriesTG: [SubCategory] = []
    private var subCategoriesVG: [SubCategory] = []
    private var tablegames: [TableGames] = []
    private var videogames: [VideoGames] = []
    private var subCategoryTG: SubCategory?
    private var subCategoryVG: SubCategory?
    private var dataType: SubCategoryViewType?
    private var gamesCell = GamesCell()
    private var position : Int = 0
    
    convenience init(clickCategory: SubCategory, typeData: SubCategoryViewType?){
        self.init()
        self.dataType = typeData
        if let type = dataType {
            switch type {
            case .tablegame:
                self.subCategoryTG = clickCategory
                self.title = subCategoryTG?.title
            case .videogame:
                self.subCategoryVG = clickCategory
                self.title = subCategoryVG?.title
            }
        }
    }
    
    init(){
        super.init(nibName: "GamesViewController", bundle: nil)
        self.tabBarItem.image = UIImage(named: "tablegamespress")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        repositoryTableGamesCategory = LocalSubCategoryRepository()
        repositoryVideoGamesCategory = LocalSubCategoryRepository()
        if let allCategoriesTG = repositoryTableGamesCategory?.getAll(type: dataType ?? .tablegame){
            subCategoriesTG = allCategoriesTG
        }
        if let allCategoriesVG = repositoryVideoGamesCategory?.getAll(type: dataType ?? .videogame) {
            subCategoriesVG = allCategoriesVG
        }
        registerCell()
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        runSubcategories()
        self.collectionView.selectItem(at: IndexPath(item: position, section: 0), animated: true, scrollPosition: .centeredHorizontally)
        //The last item don't selected
        self.collectionView(self.collectionView, didSelectItemAt: IndexPath(item: position, section: 0))
    }
    
    internal func registerCell(){
        let identifier = "GamesCell"
        let cellNib = UINib(nibName: identifier, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: identifier)
        let identifier2 = "SubMenuCollectionCell"
        let cellNib2 = UINib(nibName: identifier2, bundle: nil)
        collectionView.register(cellNib2, forCellWithReuseIdentifier: identifier2)
    }
    
    func runSubcategories(){
        if let type = dataType {
            switch type {
            case .tablegame:
                position = subCategoriesTG.index(where: {$0.id == subCategoryTG?.id}) ?? 0
            case .videogame:
                position = subCategoriesVG.index(where: {$0.id == subCategoryVG?.id}) ?? 0
            }
        }
    }
    
    func set(type typeData: SubCategoryViewType) {
        dataType = typeData
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension GamesViewController: UITableViewDelegate ,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let type = dataType {
            switch type {
            case .tablegame:
                if let count = subCategoryTG?.tablegames.count {
                    return count
                }
            case .videogame:
                if let count = subCategoryVG?.videogames.count {
                    return count
                }
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:GamesCell = tableView.dequeueReusableCell(withIdentifier: "GamesCell", for: indexPath) as! GamesCell
        if let type = dataType {
            switch type {
            case .tablegame:
                let tablegame = subCategoryTG?.tablegames[indexPath.row]
                if let image = tablegame?.image {
                    cell.gameImageView?.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "swift"), options: .cacheMemoryOnly, completed: nil)
                }
                cell.gameName?.text = tablegame?.title
                cell.gameBody?.text = tablegame?.body
                
                if let price = tablegame?.price {
                    cell.gamePrice?.text = "Precio: " + price + " €"
                }
                
                if let gameFilters = tablegame?.filters {
                    cell.filters = gameFilters
                    gamesCell.getFilters(gameFilters: gameFilters)
                }
                
                cell.tablegame = tablegame
                
                
            case .videogame:
                let videogame = subCategoryVG?.videogames[indexPath.row]
                if let image = videogame?.image {
                    cell.gameImageView?.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "swift"), options: .cacheMemoryOnly, completed: nil)
                }
                cell.gameName?.text = videogame?.title
                cell.gameBody?.text = videogame?.body
                
                if let price = videogame?.price {
                    cell.gamePrice?.text = "Precio: " + price + " €"
                }
                
                if let gameFilters = videogame?.filters {
                    cell.filters = gameFilters
                    gamesCell.getFilters(gameFilters: gameFilters)
                }
                
                cell.videogame = videogame
            }
            
        }
        return cell
    }
}
extension GamesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let type = dataType {
            switch type {
            case .tablegame:
                return subCategoriesTG.count
            case .videogame:
                return subCategoriesVG.count
            }
        }
        return 0
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:SubMenuCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubMenuCollectionCell", for: indexPath) as! SubMenuCollectionCell
        if let type = dataType {
            switch type {
            case .tablegame:
                let subCategorie = subCategoriesTG[indexPath.row]
                cell.nameCategory?.text = subCategorie.title
            case .videogame:
                let subCategorie = subCategoriesVG[indexPath.row]
                cell.nameCategory?.text = subCategorie.title
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:170, height: 50)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let type = dataType {
            switch type {
            case .tablegame:
                let cell = collectionView.cellForItem(at: indexPath)
                cell?.layer.cornerRadius = 25
                cell?.layer.backgroundColor = UIColor(red: 0.8, green: 0, blue: 0.8, alpha: 1.0).cgColor
                cell?.layer.masksToBounds = true
                let subCategory = subCategoriesTG[indexPath.row]
                if let subCategoriesTGtitle = repositoryTableGamesCategory?.get(title: subCategory.title) {
                    subCategoryTG = subCategoriesTGtitle
                }
                
                self.title = subCategoryTG?.title
                if let tableGames =  subCategoryTG?.tablegames{
                    tablegames = tableGames
                }
                tableView.reloadData()
            case .videogame:
                let cell = collectionView.cellForItem(at: indexPath)
                cell?.layer.cornerRadius = 25
                cell?.layer.backgroundColor = UIColor(red: 0.8, green: 0, blue: 0.8, alpha: 1.0).cgColor
                cell?.layer.masksToBounds = true
                let subCategory = subCategoriesVG[indexPath.row]
                if let subCategoriesVGtitle = repositoryTableGamesCategory?.get(title: subCategory.title) {
                    subCategoryVG = subCategoriesVGtitle
                }
                
                self.title = subCategoryVG?.title
                if let videoGames = subCategoryVG?.videogames{
                    videogames = videoGames
                }
                tableView.reloadData()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.cornerRadius = 25
        cell?.layer.backgroundColor = UIColor.white.cgColor
        cell?.layer.masksToBounds = true
    }
    
}

//
//  UsViewController.swift
//  ImagineGamesIOS
//
//  Created by Alberto Gurpegui Ramon on 17/02/2019.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class UsViewController: UIViewController {
    
    @IBOutlet weak var logoImageView: UIImageView?
    @IBOutlet weak var titleApp: UILabel?
    @IBOutlet weak var bodyApp: UILabel?
    @IBOutlet weak var usCollectionView: UICollectionView?
    private var integrants: [Integrants] = []
    private var integrant: Integrants?

    override func viewDidLoad() {
        super.viewDidLoad()
        logoImageView?.image = UIImage(named: "logo")
        titleApp?.text = "ImagineGames"
        bodyApp?.text = "Estos son los miembros que forman ImagineGames. Están puestos con una imagen de cada uno y sus cargos o los rasgos en los que han destacado en el proceso de creación de esta maravillosa App."
        let integrant1 = Integrants(image: "alberto", name: "Alberto", type: "Jefe de Proyecto")
        let integrant2 = Integrants(image: "pablo", name: "Pablo", type: "Base de Datos")
        let integrant3 = Integrants(image: "victor", name: "Victor", type: "Híbrido")
        let integrant4 = Integrants(image: "alvaro", name: "Alvaro", type: "Diseño")
        let integrant5 = Integrants(image: "kiran", name: "Kiran", type: "Estructuras")
        let integrant6 = Integrants(image: "guille", name: "Guille", type: "Librerias")
        integrants = [integrant1,integrant2,integrant3,integrant4,integrant5,integrant6]
        registerCell()
    }
    
    internal func registerCell(){
        let identifier = "UsCell"
        let cell = UINib(nibName: identifier, bundle: nil)
        usCollectionView?.register(cell, forCellWithReuseIdentifier: identifier)
    }
}

extension UsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return integrants.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 140, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:UsCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UsCell", for: indexPath) as! UsCell
        let integrant = integrants[indexPath.row]
        cell.integrantImageView?.image = UIImage(named: integrant.image)
        cell.nameIntegrant?.text = integrant.name
        cell.typeIntegrant?.text = integrant.type
        return cell
    }
}

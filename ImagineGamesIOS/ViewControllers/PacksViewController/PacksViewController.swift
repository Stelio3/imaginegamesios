//
//  PacksViewController.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 17/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit
import RealmSwift

class PacksViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    private var repositoryPack: LocalPacksRepository?
    private var packs: [Packs] = []
    // MARK - Init

    init(){
        super.init(nibName: "PacksViewController", bundle: nil)
         self.title = NSLocalizedString("titled_packs", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        repositoryPack = LocalPacksRepository()
        if let allPacks = repositoryPack?.getAll(){
            packs = allPacks
        }
    }
    
    internal func registerCell(){
        let identifier = "PackCell"
        let cell = UINib(nibName: identifier, bundle: nil)
        collectionView.register(cell, forCellWithReuseIdentifier: identifier)
    }
    
   
        
    
        
    }
    
    
    

extension PacksViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return packs.count - 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 375, height: 301)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:PackCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PackCell", for: indexPath) as! PackCell
        let pack = packs[indexPath.row]
        cell.packImg?.image = UIImage(named: pack.image)
        cell.packPrice?.text = "Precio: " + pack.price + "€"
        cell.packDescription?.text = pack.descriptions
        cell.packTitle?.text = pack.title
        print(pack.title)
        cell.mypacks?.title = pack.title
        print(cell.mypacks?.title)
        return cell
    }
}

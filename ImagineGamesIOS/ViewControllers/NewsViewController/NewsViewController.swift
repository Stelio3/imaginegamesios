//
//  NewsViewController.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 17/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class NewsViewController: UIViewController {

    // MARK - Init

    init(){
        super.init(nibName: "NewsViewController", bundle: nil)
//        self.tabBarItem.image = UIImage(named: "series")

        configureNavigationBar()
        
        self.title = NSLocalizedString("titled_news", comment: "")

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK - Metodos

    @IBOutlet weak var iCarouselView: iCarousel!

    var imgArr = [UIImage(named: "pack_aleatorio"),
                  UIImage(named: "pack_navidad"),
                  UIImage(named: "pack_san_valentin"),
                  UIImage(named: "pack_aleatorio"),
                  UIImage(named: "esenciales_de_mesa"),
                  UIImage(named: "esenciales_de_videojuegos")]

    override func viewDidLoad() {
        super.viewDidLoad()

        iCarouselView.type = .rotary
        iCarouselView.contentMode = .scaleAspectFill
    }

    func configureNavigationBar() {
         navigationController?.navigationBar.barTintColor = .yellow
        navigationController?.navigationBar.barStyle = .black
       
        navigationItem.title = "news"
    }
}

extension NewsViewController: iCarouselDataSource, iCarouselDelegate{
    func numberOfItems(in carousel: iCarousel) -> Int {
        return imgArr.count
    }

    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var imageView: UIImageView!
        if view == nil {
            imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 650, height: 300))
            imageView.contentMode = .scaleAspectFit
        }else{
            imageView = view as? UIImageView
        }

        imageView.image = imgArr[index]
        return imageView
    }
}


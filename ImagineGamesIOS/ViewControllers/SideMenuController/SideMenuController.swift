//
//  SideMenuController.swift
//  ImagineGamesIOS
//
//  Created by VICTOR ALVAREZ LANTARON on 6/2/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//
import UIKit

class SideMenuController: UIViewController {
    internal var arrItems = ["Cesta","Nosotros","Español","English"]
    internal var delegate: ContainerNavigationController?
    
    @IBOutlet var backView: UIView!
    @IBOutlet var tableView : UITableView!
    @IBOutlet var imgIconApp: UIImageView!
    
    override func viewDidLoad() {
        registerCell()
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
        leftSwipe.direction = .left
        
        backView.addGestureRecognizer(leftSwipe)
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 0.6){
            self.view.center.x += self.view.bounds.width
            
        }
    }
    
    @objc func handleSwipe(sender: UISwipeGestureRecognizer){
        if sender.state == .ended {
            switch sender.direction{
            case .left:
                print("ATRASSSSSS")
                
//                                let transition: CATransition = CATransition()
//                                transition.duration = 0.2
//                
//                                self.view.window!.layer.add(transition, forKey: nil)
                self.dismiss(animated: false, completion: nil)
            default:
                break
            }
        }
    }
    
    // set the cell of the table
    internal func registerCell(){
        let identifier = "SideMenuCell"
        let nib = UINib(nibName: identifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: identifier)
    }
}

extension SideMenuController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SideMenuCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
        
        cell.lblTitle.text = arrItems[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected = arrItems[indexPath.row]
        switch selected {
        case "Cesta":
            self.dismiss(animated: false, completion: nil)
            delegate?.goBasket()
        case "Nosotros":
            self.dismiss(animated: false, completion: nil)
            delegate?.goUs()
        default:
            print(arrItems[indexPath.row])
        }
    }
}

//
//  SubCategoryTableGamesViewController.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 22/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit
import RealmSwift
import SDWebImage

class SubCategoryViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var repositoryCategory: LocalSubCategoryRepository?
    var navigation: NavigationController?
    
    private var subCategories: [SubCategory] = []
    private let realm = try! Realm()
    
    private var subCategory:SubCategory?
    private var dataType: SubCategoryViewType?
    

    init(){
        super.init(nibName: "SubCategoryViewController", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(type typeData: SubCategoryViewType, title: String) {
        dataType = typeData
        navigation?.setTitle(title: title)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        repositoryCategory = LocalSubCategoryRepository()
        if let allSubCategories = repositoryCategory?.getAll(type: dataType ?? .videogame){
            subCategories = allSubCategories
        }
        tableView.reloadData()
    }
    
    
    internal func registerCell(){
        let identifier = "SubCategoryCell"
        let cellNib = UINib(nibName: identifier, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: identifier)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


extension SubCategoryViewController: UITableViewDelegate ,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell { 
        let cell:SubCategoryCell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoryCell
        
        if let type = dataType {
            switch type {
                case .tablegame:
                    let subCategoryTG = subCategories[indexPath.row]
                    cell.categoryImageView?.sd_setImage(with: URL(string: subCategoryTG.image), placeholderImage: UIImage(named: subCategoryTG.image), options: .cacheMemoryOnly, completed: nil)
                    cell.categoryName?.text = subCategoryTG.title
                
                case .videogame:
                    let subCategoryVG = subCategories[indexPath.row]
                    cell.categoryImageView?.sd_setImage(with: URL(string: subCategoryVG.image), placeholderImage: UIImage(named: subCategoryVG.image), options: .cacheMemoryOnly, completed: nil)
                    cell.categoryName?.text = subCategoryVG.title
            }
        }

        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let type = dataType {
            switch type {
            case .tablegame:
                let subCategorieTG = subCategories[indexPath.row]
                let tablegamesVC = GamesViewController(clickCategory: subCategorieTG, typeData: dataType)
                navigationController?.pushViewController(tablegamesVC, animated: true)
                
            case .videogame:
                let subCategorieVG = subCategories[indexPath.row]
                let videogamesVC = GamesViewController(clickCategory: subCategorieVG, typeData: dataType)
                navigationController?.pushViewController(videogamesVC, animated: true)
            }
        }
    }
}

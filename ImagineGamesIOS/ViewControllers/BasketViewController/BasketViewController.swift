//
//  BasketViewController.swift
//  ImagineGamesIOS
//
//  Created by GUILLERMO CRESPO AGUAYO on 13/2/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit
import SDWebImage

class BasketViewController: UIViewController {
    @IBOutlet weak var basketList: UITableView?
    var session = SingletonBasket.sharedInstance
    var total:Int?
    var totalPacks:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
    }

    private func registerCell(){
        let identifier = "BasketCell"
        let cellNib = UINib(nibName: identifier, bundle: nil)
        basketList?.register(cellNib, forCellReuseIdentifier: identifier)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension BasketViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {
        switch(section) {
        case 0:
            return "Games"
        case 1:
            return "Packs"
        default :
            return ""
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section==0){
            total = session.getGames()
            if(total != nil && total != 0){
                print("total",total)
                return total!
            }else{
                return 1
            }
            
        }else if(section == 1){
            totalPacks = session.getPacks()
            if(totalPacks != nil && totalPacks != 0){
                print("totalPacks",totalPacks)
                return totalPacks!
            }else{
                return 1
            }
        }
        return 0
    }
    
    func tableView(_ tableView:UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    func tableView (_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell = UITableViewCell()
        if(indexPath.section == 0){
            cell = createGamesSecction(indexPath)
        }
        else if(indexPath.section == 1){
            cell = createPacksSecction(indexPath)
        }
        
        return cell
    }
    
    //Games Secction
    func createGamesSecction(_ indexPath: IndexPath) -> BasketCell{
        let cell: BasketCell = basketList?.dequeueReusableCell(withIdentifier: "BasketCell", for: indexPath) as! BasketCell
            if(total == 0){
                cell.lblTitle?.text = "No hay elementos"
                cell.lblPrize?.text = ""
                cell.lblQuantity?.text = ""
                cell.btnMinus?.setImage(nil, for: .normal)
                cell.btnPlus?.setImage(nil, for: .normal)
            }else{
                let game = session.arrGames[indexPath.row]
                cell.lblTitle?.text = game.title
                cell.imageProduct?.sd_setImage(with: URL(string: game.image), completed: nil)
                cell.lblPrize?.text = game.price + "€"
                cell.lblQuantity?.text = String(game.numberItems)
        }
    
            cell.selectionStyle = .none
    
        return cell
    }
    
    
    //Packs Seccition
    func createPacksSecction(_ indexPath: IndexPath) -> BasketCell{
        let cell: BasketCell = basketList?.dequeueReusableCell(withIdentifier: "BasketCell", for: indexPath) as! BasketCell
        if(totalPacks == 0){
            cell.lblTitle?.text = "No hay elementos"
            cell.lblPrize?.text = ""
            cell.lblQuantity?.text = ""
            cell.btnMinus?.setImage(nil, for: .normal)
            cell.btnPlus?.setImage(nil, for: .normal)
        }else{
            
            let packs = session.arrPacks[indexPath.row]
            print("packs title", packs.title)
            cell.lblTitle?.text = packs.title
            cell.imageProduct?.image = UIImage(named: packs.image)
            cell.lblPrize?.text = packs.price + "€"
            cell.lblQuantity?.text = String (packs.count)
        }
        cell.selectionStyle = .none
        return cell
    }
}


 
    


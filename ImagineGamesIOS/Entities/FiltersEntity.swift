//
//  FiltersEntity.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 21/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import Foundation
import RealmSwift

class FilterEntity: Object {
    
    @objc dynamic var id = ""
    @objc dynamic var image = ""
    @objc dynamic var title = ""
    
    override static func primaryKey() -> String?{
        return "id"
    }
    
    convenience init(id: String = "", image:String = "", title: String = "") {
        self.init()
        self.id = id
        self.image = image
        self.title = title
    }
    
    func filtersModel() -> Filter {
        let model = Filter()
        model.id = id
        model.image = image
        model.title = title
        return model
    }
    
}

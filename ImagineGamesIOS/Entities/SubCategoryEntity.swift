//
//  CategoryTableGames.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 18/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import Foundation
import RealmSwift

class SubCategoryEntity: Object {
    
    @objc dynamic var id = ""
    @objc dynamic var image = ""
    @objc dynamic var title = ""
    var tablegames: List<TableGamesEntity> = List<TableGamesEntity>()
    var videogames: List<VideoGamesEntity> = List<VideoGamesEntity>()
    
    override static func primaryKey() -> String?{
        return "id"
    }
    
    convenience init(subcategory: SubCategory) {
        self.init()
        self.id = subcategory.id
        self.image = subcategory.image
        self.title = subcategory.title
        for tablegame in subcategory.tablegames {
            self.tablegames.append(tablegame.tablegamesEntity())
        }
        for videogame in subcategory.videogames {
            self.videogames.append(videogame.videogamesEntity())
        }
    }
    
    func subcategoryModel() -> SubCategory {
        let model = SubCategory()
        model.id = id
        model.image = image
        model.title = title
        for tablegame in self.tablegames {
            model.tablegames.append(tablegame.tablegamesModel())
        }
        for videogame in self.videogames {
            model.videogames.append(videogame.videogamesModel())
        }
        return model
    }
    
}

//
//  PaksEntity.swift
//  ImagineGamesIOS
//
//  Created by Alberto Gurpegui Ramon on 15/02/2019.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import Foundation
import RealmSwift

class PacksEntity: Object {
    
    @objc dynamic var id = ""
    @objc dynamic var image = ""
    @objc dynamic var title = ""
    @objc dynamic var descriptions = ""
    @objc dynamic var price = ""
    @objc dynamic var count = 0
    var tablegamesPacks: List<TableGamesEntity> = List<TableGamesEntity>()
    var videogamesPacks: List<VideoGamesEntity> = List<VideoGamesEntity>()
    
    override static func primaryKey() -> String?{
        return "id"
    }
    
    convenience init(packs: Packs) {
        self.init()
        self.id = packs.id
        self.image = packs.image
        self.title = packs.title
        self.descriptions = packs.descriptions
        self.price = packs.price
        self.count = packs.count
        for tablegame in packs.tablegamesPacks {
            self.tablegamesPacks.append(tablegame.tablegamesEntity())
        }
        for videogame in packs.videogamesPacks {
            self.videogamesPacks.append(videogame.videogamesEntity())
        }
    }
    
    func packsModel() -> Packs {
        let model = Packs()
        model.id = id
        model.image = image
        model.title = title
        model.descriptions = descriptions
        model.price = price
        model.count = count
        for tablegame in self.tablegamesPacks {
            model.tablegamesPacks.append(tablegame.tablegamesModel())
        }
        for videogame in self.videogamesPacks {
            model.videogamesPacks.append(videogame.videogamesModel())
        }
        return model
    }
    
}

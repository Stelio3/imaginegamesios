//
//  VideoGames.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 18/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import Foundation
import RealmSwift

class VideoGamesEntity: Object {
    
    @objc dynamic var id = ""
    @objc dynamic var image = ""
    @objc dynamic var title = ""
    @objc dynamic var body = ""
    @objc dynamic var price = ""
    @objc dynamic var numberItems = 0
    var filters: List<FilterEntity> = List<FilterEntity>()
    
    override static func primaryKey() -> String?{
        return "id"
    }
    
    convenience init(id: String = "", image: String = "", title: String = "", body: String = "", price: String = "", numberItems: Int = 0, filters: [Filter] = []) {
        self.init()
        self.id = id
        self.image = image
        self.title = title
        self.body = body
        self.price = price
        self.numberItems = numberItems
        for filter in filters {
            self.filters.append(filter.filterEntity())
        }
    }
    
    func videogamesModel() -> VideoGames {
        let model = VideoGames()
        model.id = id
        model.image = image
        model.title = title
        model.body = body
        model.price = price
        model.numberItems = numberItems
        for filter in self.filters {
            model.filters.append(filter.filterModel())
        }
        return model
    }
    
}

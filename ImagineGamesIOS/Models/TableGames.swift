//
//  TableGames.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 18/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class TableGames {
    var id:String = ""
    var image:String = ""
    var title:String = ""
    var body:String = ""
    var price: String = ""
    var numberItems: Int = 0
    var filters: [Filter] = []
    
    convenience init(id: String, image: String, title: String, body: String, price: String, numberItems: Int, filters: [Filter]) {
        self.init()
        self.id = id
        self.image = image
        self.title = title
        self.body = body
        self.price = price
        self.numberItems = numberItems
        self.filters = filters
    }
    
    func tablegamesEntity() -> TableGamesEntity {
        let entity = TableGamesEntity(id: id,
                                      image: image,
                                      title: title,
                                      body: body,
                                      price: price,
                                      numberItems: numberItems,
                                      filters: filters)
        return entity
    }
}

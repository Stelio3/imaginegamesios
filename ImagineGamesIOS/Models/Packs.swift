//
//  Packs.swift
//  ImagineGamesIOS
//
//  Created by Alberto Gurpegui Ramon on 15/02/2019.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class Packs {
    var id:String = ""
    var image:String = ""
    var title:String = ""
    var descriptions:String = ""
    var price:String = ""
    var count:Int = 0
    var tablegamesPacks: [TableGames] = []
    var videogamesPacks: [VideoGames] = []
    
    convenience init(id: String, image: String, title: String, descriptions:String, price:String, count:Int, tablegamesPacks: [TableGames], videogamesPacks: [VideoGames]) {
        self.init()
        self.id = id
        self.image = image
        self.title = title
        self.descriptions = descriptions
        self.price = price
        self.count = count
        self.tablegamesPacks = tablegamesPacks
        self.videogamesPacks = videogamesPacks
    }
    
    func packsEntity() -> Packs {
        let entity = Packs()
        entity.id = self.id
        entity.image = self.image
        entity.title = self.title
        entity.descriptions = self.descriptions
        entity.price = self.price
        entity.count = self.count
        entity.tablegamesPacks = self.tablegamesPacks
        entity.videogamesPacks = self.videogamesPacks
        return entity
    }
}

//
//  Filters.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 21/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class Filter {
    var id:String = ""
    var image:String = ""
    var title:String = ""
    
    convenience init(id: String, image: String, title: String) {
        self.init()
        self.id = id
        self.image = image
        self.title = title
    }
    
    func filterEntity() -> FilterEntity {
        let entity = FilterEntity()
        entity.id = self.id
        entity.image = self.image
        entity.title = self.title
        return entity
    }
}

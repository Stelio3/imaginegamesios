//
//  Games.swift
//  ImagineGamesIOS
//
//  Created by Guille Crespo Aguayo on 16/02/2019.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class Games {
    var id: String = ""
    var image: String = ""
    var title: String = ""
    var body: String = ""
    var price: String = ""
    var numberItems: Int = 0
   // var filters: [Filter] = []
    
    convenience init(id: String, image: String, title: String, body: String, price: String, numberItems: Int) {
        self.init()
        self.id = id
        self.image = image
        self.title = title
        self.body = body
        self.price = price
        self.numberItems = numberItems
      //  self.filters = filters
    }
}

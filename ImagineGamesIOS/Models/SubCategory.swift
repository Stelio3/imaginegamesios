//
//  CategoryTableGames.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 18/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class SubCategory {
    var id: String = ""
    var image: String = ""
    var title: String = ""
    var tablegames: [TableGames] = []
    var videogames: [VideoGames] = []
    
    convenience init(id: String, image: String, title: String, tablegames: [TableGames], videogames: [VideoGames]) {
        self.init()
        self.id = id
        self.image = image
        self.title = title
        self.tablegames = tablegames
        self.videogames = videogames
    }

    func subcategorytablegamesEntity() -> SubCategory {
        let entity = SubCategory()
        entity.id = self.id
        entity.image = self.image
        entity.title = self.title
        entity.tablegames = self.tablegames
        entity.videogames = self.videogames
        return entity
    }
}

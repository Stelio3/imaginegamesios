//
//  Integrants.swift
//  ImagineGamesIOS
//
//  Created by Alberto Gurpegui Ramon on 17/02/2019.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import Foundation

class Integrants {
    var image: String = ""
    var name: String = ""
    var type: String = ""
    
    convenience init(image: String, name: String, type: String) {
        self.init()
        self.image = image
        self.name = name
        self.type = type
    }
}

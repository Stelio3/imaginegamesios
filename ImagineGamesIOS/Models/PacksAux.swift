//
//  PacksAux.swift
//  ImagineGamesIOS
//
//  Created by Guille Crespo Aguayo on 17/02/2019.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class PacksAux {
    var id:String = ""
    var image:String = ""
    var title:String = ""
    var descriptions:String = ""
    var price:String = ""
    var count:String = ""
  
    
    convenience init(id: String, image: String, title: String, descriptions:String, price:String, count:String) {
        self.init()
        self.id = id
        self.image = image
        self.title = title
        self.descriptions = descriptions
        self.price = price
        self.count = count
       
}
}

//
//  NavigatorController.swift
//  ImagineGamesIOS
//
//  Created by PABLO HERNANDEZ JIMENEZ on 15/2/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import Foundation

class NavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    func setTitle(title: String) {
        switch title {
        case "titled_tablegames":
            self.title = NSLocalizedString("titled_tablegames", comment: "")
        case "titled_videogames":
            self.title = NSLocalizedString("titled_videogames", comment: "")
        default:
            break
        }
    }
}

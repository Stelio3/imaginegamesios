//
//  TabBarController.swift
//  ImagineGamesIOS
//
//  Created by VICTOR ALVAREZ LANTARON on 6/2/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class TabBarNavigationController: UITabBarController {
    
    var delegateNav: ContainerNavigationController?
    
    private var firstTabNavigationController : UINavigationController!
    private var secondTabNavigationControoller : UINavigationController!
    private var thirdTabNavigationController : UINavigationController!
    private var fourthTabNavigationControoller : UINavigationController!
    private var fifthTabNavigationController : UINavigationController!
    
    override func viewDidLoad() {
        UITabBar.appearance().tintColor = UIColor(red: 0.5, green: 0, blue: 0.5, alpha: 1)
        UITabBar.appearance().backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(233.0/255.0), blue: CGFloat(138.0/255.0), alpha: CGFloat(1.0))
        let subCategoryTableGamesVC = SubCategoryViewController()
        let subCategoryVideoGamesVC = SubCategoryViewController()
        let newsVC = NewsViewController()
        let packsVC = PacksViewController()
        let filtersVC = FiltersViewController()
        subCategoryTableGamesVC.set(type: .tablegame, title: "titled_tablegames")
        subCategoryVideoGamesVC.set(type: .videogame, title: "titled_videogames")
        
        subCategoryTableGamesVC.title = NSLocalizedString("titled_tablegames", comment: "")
        subCategoryVideoGamesVC.title = NSLocalizedString("titled_videogames", comment: "")
        subCategoryTableGamesVC.tabBarItem.image = UIImage(named: "tablegamespress")
        subCategoryVideoGamesVC.tabBarItem.image = UIImage(named: "videogamespress")
        
        firstTabNavigationController = UINavigationController.init(rootViewController: newsVC)
        secondTabNavigationControoller = UINavigationController.init(rootViewController: subCategoryTableGamesVC)
        thirdTabNavigationController = UINavigationController.init(rootViewController: subCategoryVideoGamesVC)
        fourthTabNavigationControoller = UINavigationController.init(rootViewController: packsVC)
        fifthTabNavigationController = UINavigationController.init(rootViewController: filtersVC)
        
        self.viewControllers = [firstTabNavigationController, secondTabNavigationControoller, thirdTabNavigationController, fourthTabNavigationControoller, fifthTabNavigationController]
        
        let item1 = UITabBarItem(title: "Novedades", image: UIImage(named: "news"), tag: 0)
        let item4 = UITabBarItem(title: "Paquetes", image:  UIImage(named: "packs"), tag: 3)
        let item5 = UITabBarItem(title: "Filtros", image:  UIImage(named: "filters"), tag: 4)
        
        firstTabNavigationController.tabBarItem = item1
        /*secondTabNavigationControoller.tabBarItem = item2
        thirdTabNavigationController.tabBarItem = item3*/
        fourthTabNavigationControoller.tabBarItem = item4
        fifthTabNavigationController.tabBarItem = item5
        putAllData()
        setListenerTabBar()
    }
    
    func setListenerTabBar(){
        NotificationCenter.default.addObserver(self, selector: #selector(goBasket), name: NSNotification.Name("goBasket"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(goUs), name: NSNotification.Name("goUs"), object: nil)
    }
    
    
    @IBAction func onMoreTapped() {
        print("sidee menuuu")
        
        // observer que indica que ya se puede abrir el navegador
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil )
    }
    
    @IBAction func changeIdiom() {
        let basketVC = BasketViewController()
        navigationController?.pushViewController(basketVC, animated: false)
    }
    
    @objc internal func goBasket () {
        let basketVC = BasketViewController()
        navigationController?.pushViewController(basketVC, animated: true)
    }
    
    @objc internal func goUs () {
        let usVC = UsViewController()
        navigationController?.pushViewController(usVC, animated: true)
    }
}

//
//  ContainerNavigationController.swift
//  ImagineGamesIOS
//
//  Created by VICTOR ALVAREZ LANTARON on 6/2/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class ContainerNavigationController: UIViewController {
        
    var sideMenuViewController: SideMenuController!
    var sideMenuOpen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuViewController = SideMenuController()
        setListeners()
    }
    
    func setListeners(){
        NotificationCenter.default.addObserver(self, selector: #selector(toggleSideMenu), name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }
    
    func goUs(){
        NotificationCenter.default.post(name: NSNotification.Name("goUs"), object: nil )
    }
    
    func goBasket(){
        NotificationCenter.default.post(name: NSNotification.Name("goBasket"), object: nil )
    }
    
    @objc internal func toggleSideMenu () {
        let addVC = SideMenuController()
        addVC.delegate = self
        addVC.modalTransitionStyle = .coverVertical
        addVC.modalPresentationStyle = .overCurrentContext
        present(addVC, animated: true, completion: nil)
        
    }
}

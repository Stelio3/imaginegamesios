//
//  UsCell.swift
//  ImagineGamesIOS
//
//  Created by Alberto Gurpegui Ramon on 17/02/2019.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class UsCell: UICollectionViewCell {
    
    @IBOutlet weak var integrantImageView: UIImageView?
    @IBOutlet weak var nameIntegrant: UILabel?
    @IBOutlet weak var typeIntegrant: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}

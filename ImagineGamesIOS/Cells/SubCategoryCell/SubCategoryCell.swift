//
//  SubCategoryTableGamesCell.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 22/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class SubCategoryCell: UITableViewCell {
    
    @IBOutlet weak var categoryView: UIView?
    @IBOutlet weak var categoryImageView:UIImageView?
    @IBOutlet weak var categoryName:UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        categoryView?.layer.cornerRadius = 25
        categoryView?.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

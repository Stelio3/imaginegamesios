//
//  BasketCell.swift
//  ImagineGamesIOS
//
//  Created by GUILLERMO CRESPO AGUAYO on 13/2/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class BasketCell: UITableViewCell {
    @IBOutlet var lblTitle:UILabel?
    @IBOutlet var lblPrize:UILabel?
    @IBOutlet var lblQuantity:UILabel?
    @IBOutlet var imageProduct: UIImageView?
    @IBOutlet var btnPlus:UIButton?
    @IBOutlet var btnMinus:UIButton?
    var quantityAux:Int?
    var session =  SingletonBasket.sharedInstance
    
    @IBAction func plusQuantity() {
           addQuantity()
       
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        quantityAux = 1;
        let imageMinus = UIImage(named: "minus")
        btnMinus?.setImage(imageMinus, for: .normal)
        let imagePlus = UIImage(named: "plus")
        btnPlus?.setImage(imagePlus, for: .normal)
        
     
    }
    
    func addQuantity(){
        quantityAux = +1
        lblQuantity?.text = String(quantityAux!)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

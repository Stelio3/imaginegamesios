//
//  TableGamesCell.swift
//  ImagineGamesIOS
//
//  Created by Alberto Gurpegui Ramon on 31/01/2019.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class GamesCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var gameFiltersCollectionView: UICollectionView!
    @IBOutlet weak var gameImageView:UIImageView?
    @IBOutlet weak var gameName:UILabel?
    @IBOutlet weak var gameBody:UILabel?
    @IBOutlet weak var gamePrice:UILabel?
    @IBOutlet weak var gameButton:UIButton?
    
    var session = SingletonBasket.sharedInstance
    
    var videogame: VideoGames?
    var tablegame: TableGames?
    var filters: [Filter] = []
    
    
    
    let cellReuseId = "FiltersCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        gameFiltersCollectionView?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "FiltersCell")
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 70, height: 80)
        flowLayout.minimumLineSpacing = 5.0
        flowLayout.minimumInteritemSpacing = 5.0
        self.gameFiltersCollectionView?.collectionViewLayout = flowLayout
        gameFiltersCollectionView?.backgroundColor = UIColor.yellow
        
    }
    
    func getFilters(gameFilters: [Filter]) {
        filters = gameFilters
    }
    
    @IBAction func addGamestoBasket(){
        if (gameName?.text) != nil {
            
            if let price = gamePrice?.text{
                print("********" + price)
                
                if let image = gameImageView?.image{
                    print("SELECTORRRRRRRRR")
                    
                    if let game = videogame {
                        let mgame = Games(id: game.id, image: game.image, title: game.title, body: game.body, price: game.price, numberItems: game.numberItems+1)
                        print("___________" + game.title + " insertado")
                        
                        
                            session.addGame(game: mgame)
                        
                        
                        print(image)
                    } else if let game = tablegame {
                        let mgame = Games(id: game.id, image: game.image, title: game.title, body: game.body, price: game.price, numberItems: game.numberItems+1)
                        print("___________" + game.title + " insertado")
                        session.addGame(game: mgame)
                        print(image)
                    } else {
                        print("aaaaaaaaaaaaaaaaaaaaaaaaaaa no funciona")
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filters.count - 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:20, height: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: FiltersCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FiltersCell", for: indexPath) as! FiltersCell
        let filter = filters[indexPath.row]
        cell.imgFilter?.image = UIImage(named: filter.image)
        return cell
    }
}

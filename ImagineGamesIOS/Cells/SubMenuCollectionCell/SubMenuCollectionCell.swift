//
//  SubMenuCollectionCell.swift
//  ImagineGamesIOS
//
//  Created by Alberto Gurpegui Ramon on 04/02/2019.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import UIKit

class SubMenuCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var nameCategory: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let name = nameCategory {
            name.layer.cornerRadius = 5
            name.layer.masksToBounds = true
        }
        // Initialization code
    }
}

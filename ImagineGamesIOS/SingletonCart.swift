//
//  SingletonCart.swift
//  ImagineGamesIOS
//
//  Created by GUILLERMO CRESPO AGUAYO on 13/2/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import Foundation
import UIKit

class SingletonBasket {
    
    static let sharedInstance = SingletonBasket()
 
    var arrGames:[Games] = []
    var arrPacks:[Packs] = []
    var games:Games?
    var mCheck:Bool = false
    var sizePack:Int = 0
    
    func addGame(game: Games){
        mCheck = false
        if(SingletonBasket.sharedInstance.arrGames.isEmpty){
            SingletonBasket.sharedInstance.arrGames.append(game)
        }else{
            
            for element in SingletonBasket.sharedInstance.arrGames {
                if(game.title == element.title){
                    mCheck = true
                    
                    print(element.title)
                    
                }
                
            }
            if(!mCheck){
                SingletonBasket.sharedInstance.arrGames.append(game)
                
            }
        }
  
    }
    
    func getGames()-> Int{
        return SingletonBasket.sharedInstance.arrGames.count
    }
    
    func addPacks(packs: Packs){
        mCheck = false
        if(SingletonBasket.sharedInstance.arrPacks.isEmpty){
            SingletonBasket.sharedInstance.arrPacks.append(packs)
        }else{
            
            for element in SingletonBasket.sharedInstance.arrPacks {
                if(packs.title == element.title){
                    mCheck = true
                    
                    print(element.title)
                    
                }
                
            }
            if(!mCheck){
                SingletonBasket.sharedInstance.arrPacks.append(packs)
                
            }
        }
       
    }

    func  getPacks() -> Int{
        return SingletonBasket.sharedInstance.arrPacks.count
    }
    
    func isInBasket(){
        for element in SingletonBasket.sharedInstance.arrGames {
            print(element)
        }
        
    }
}

 
    


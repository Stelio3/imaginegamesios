//
//  Repository.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 18/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import Foundation

protocol Repository {
    associatedtype T
    
    func getAll() -> [T]
    func get(identifier:String) -> T?
    func get(title:String) -> T?
    func create(a:T) -> Bool
    func delete(a:T) -> Bool
    func update(a:T) -> Bool
}

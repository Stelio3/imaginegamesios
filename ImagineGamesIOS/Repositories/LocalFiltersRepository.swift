//
//  LocalFiltersRepository.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 21/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import Foundation
import RealmSwift

class LocalFilterRepository: Repository {
    
    func getAll() -> [Filter] {
        var filters: [Filter] = []
        do {
            let entities = try Realm().objects(FilterEntity.self).sorted(byKeyPath: "id", ascending: false)
            for entity in entities {
                let model = entity.filterModel()
                filters.append(model)
            }
        }
        catch let error as NSError {
            print("ERROR getAll Participants: ", error.description)
        }
        return filters
    }
    
    func get(identifier: String) -> Filter? {
        do{
            let realm = try Realm()
            if let entity = realm.objects(FilterEntity.self).filter("id == %@", identifier).first{
                let model = entity.filterModel()
                return model
            }
        }
        catch {
            return nil
        }
        return nil
    }
    
    func get(title: String) -> Filter? {
        do{
            let realm = try Realm()
            if let entity = realm.objects(FilterEntity.self).filter("id == %@", title).first{
                let model = entity.filterModel()
                return model
            }
        }
        catch {
            return nil
        }
        return nil
    }
    
    func create(a: Filter) -> Bool {
        do{
            let realm = try Realm()
            let entity = FilterEntity()
            try realm.write{
                realm.add(entity,update: true)
            }
        }
        catch{
            return false
        }
        return true
    }
    
    func delete(a: Filter) -> Bool {
        do{
            let realm = try Realm()
            try realm.write {
                let entityToDelete = realm.objects(FilterEntity.self).filter("id == %@", a.id)
                realm.delete(entityToDelete)
            }
        }
        catch{
            return false
        }
        return true
    }
    
    func update(a: Filter) -> Bool {
        return create(a:a)
    }
}

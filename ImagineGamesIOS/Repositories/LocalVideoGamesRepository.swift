//
//  LocalVideoGamesRepository.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 18/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import Foundation
import RealmSwift

class LocalVideoGamesRepository: Repository {
    
    func getAll() -> [VideoGames] {
        var videogames: [VideoGames] = []
        do {
            let entities = try Realm().objects(VideoGamesEntity.self).sorted(byKeyPath: "id", ascending: false)
            for entity in entities {
                let model = entity.videogamesModel()
                videogames.append(model)
            }
        }
        catch let error as NSError {
            print("ERROR getAll Participants: ", error.description)
        }
        return videogames
    }
    
    func get(identifier: String) -> VideoGames? {
        do{
            let realm = try Realm()
            if let entity = realm.objects(VideoGamesEntity.self).filter("id == %@", identifier).first{
                let model = entity.videogamesModel()
                return model
            }
        }
        catch {
            return nil
        }
        return nil
    }
    
    func get(title: String) -> VideoGames? {
        do{
            let realm = try Realm()
            if let entity = realm.objects(VideoGamesEntity.self).filter("title == %@", title).first{
                let model = entity.videogamesModel()
                return model
            }
        }
        catch {
            return nil
        }
        return nil
    }
    
    func create(a: VideoGames) -> Bool {
        do{
            let realm = try Realm()
            let entity = VideoGamesEntity()
            try realm.write{
                realm.add(entity,update: true)
            }
        }
        catch{
            return false
        }
        return true
    }
    
    func delete(a: VideoGames) -> Bool {
        do{
            let realm = try Realm()
            try realm.write {
                let entityToDelete = realm.objects(VideoGamesEntity.self).filter("id == %@", a.id)
                realm.delete(entityToDelete)
            }
        }
        catch{
            return false
        }
        return true
    }
    
    func update(a: VideoGames) -> Bool {
        return create(a:a)
    }
}

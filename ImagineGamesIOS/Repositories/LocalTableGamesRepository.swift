//
//  LocalTableGamesRepository.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 18/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import Foundation
import RealmSwift

class LocalTableGamesRepository: Repository {
    
    func getAll() -> [TableGames] {
        var tablegames: [TableGames] = []
        do {
            let entities = try Realm().objects(TableGamesEntity.self).sorted(byKeyPath: "id", ascending: false)
            for entity in entities {
                let model = entity.tablegamesModel()
                tablegames.append(model)
            }
        }
        catch let error as NSError {
            print("ERROR getAll Participants: ", error.description)
        }
        return tablegames
    }
    
    func get(identifier: String) -> TableGames? {
        do{
            let realm = try Realm()
            if let entity = realm.objects(TableGamesEntity.self).filter("id == %@", identifier).first{
                let model = entity.tablegamesModel()
                return model
            }
        }
        catch {
            return nil
        }
        return nil
    }
    
    func get(title: String) -> TableGames? {
        do{
            let realm = try Realm()
            if let entity = realm.objects(TableGamesEntity.self).filter("title == %@", title).first{
                let model = entity.tablegamesModel()
                return model
            }
        }
        catch {
            return nil
        }
        return nil
    }
    
    func create(a: TableGames) -> Bool {
        do{
            let realm = try Realm()
            let entity = TableGamesEntity()
            try realm.write{
                realm.add(entity,update: true)
            }
        }
        catch{
            return false
        }
        return true
    }
    
    func delete(a: TableGames) -> Bool {
        do{
            let realm = try Realm()
            try realm.write {
                let entityToDelete = realm.objects(TableGamesEntity.self).filter("id == %@", a.id)
                realm.delete(entityToDelete)
            }
        }
        catch{
            return false
        }
        return true
    }
    
    func update(a: TableGames) -> Bool {
        return create(a:a)
    }
}

//
//  LocalCategoryTableGamesRepository.swift
//  ImagineGamesIOS
//
//  Created by ALBERTO GURPEGUI RAMÓN on 18/1/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import Foundation
import RealmSwift

class LocalSubCategoryRepository: RepositoryOnlySubCategories {
    
    private var dataType: SubCategoryViewType?
    
    func getAll(type typeData: SubCategoryViewType) -> [SubCategory] {
        var subCategories: [SubCategory] = []
        dataType = typeData
        do {
            if let type = dataType {
                switch type {
                case .tablegame:
                    let entities = try Realm().objects(SubCategoryEntity.self).filter("tablegames.@count > 0")
                    for entity in entities {
                        let model = entity.subcategoryModel()
                        subCategories.append(model)
                    }
                    
                case .videogame:
                    let entities = try Realm().objects(SubCategoryEntity.self).filter("videogames.@count > 0")
                    for entity in entities {
                        let model = entity.subcategoryModel()
                        subCategories.append(model)
                    }
                }
            }
        }
        catch let error as NSError {
            print("ERROR getAll Participants: ", error.description)
        }
        return subCategories
    }
    
    func get(identifier: String) -> SubCategory? {
        do{
            let realm = try Realm()
            if let entity = realm.objects(SubCategoryEntity.self).filter("id == %@", identifier).first{
                let model = entity.subcategoryModel()
                return model
            }
        }
        catch {
            return nil
        }
        return nil
    }
    
    func get(title: String) -> SubCategory? {
        do{
            let realm = try Realm()
            if let entity = realm.objects(SubCategoryEntity.self).filter("title == %@", title).first{
                let model = entity.subcategoryModel()
                return model
            }
        }
        catch {
            return nil
        }
        return nil
    }
    
    func create(a: SubCategory) -> Bool {
        do{
            let realm = try Realm()
            let entity = SubCategoryEntity(subcategory: a)
            try realm.write{
                realm.add(entity,update: true)
            }
        }
        catch{
            return false
        }
        return true
    }
    
    func delete(a: SubCategory) -> Bool {
        do{
            let realm = try Realm()
            try realm.write {
                let entityToDelete = realm.objects(SubCategoryEntity.self).filter("id == %@", a.id)
                realm.delete(entityToDelete)
            }
        }
        catch{
            return false
        }
        return true
    }
    
    func update(a: SubCategory) -> Bool {
        return create(a:a)
    }
}

//
//  RepositoryOnlySubCategories.swift
//  ImagineGamesIOS
//
//  Created by PABLO HERNANDEZ JIMENEZ on 14/2/19.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import Foundation

protocol RepositoryOnlySubCategories {
    associatedtype T
    
    func getAll(type typeData: SubCategoryViewType) -> [T]
    func get(identifier:String) -> T?
    func get(title:String) -> T?
    func create(a:T) -> Bool
    func delete(a:T) -> Bool
    func update(a:T) -> Bool
}

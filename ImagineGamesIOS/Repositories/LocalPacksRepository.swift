//
//  LocalPacksRepository.swift
//  ImagineGamesIOS
//
//  Created by Alberto Gurpegui Ramon on 15/02/2019.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import Foundation
import RealmSwift

class LocalPacksRepository: Repository{
    
    func getAll() -> [Packs] {
        var packs: [Packs] = []
        do {
            let entities = try Realm().objects(PacksEntity.self).sorted(byKeyPath: "id", ascending: false)
            for entity in entities {
                let model = entity.packsModel()
                packs.append(model)
            }
        }
        catch let error as NSError {
            print("ERROR getAll Participants: ", error.description)
        }
        return packs
    }
    
    func get(identifier: String) -> Packs? {
        do{
            let realm = try Realm()
            if let entity = realm.objects(PacksEntity.self).filter("id == %@", identifier).first{
                let model = entity.packsModel()
                return model
            }
        }
        catch {
            return nil
        }
        return nil
    }
    
    func get(title: String) -> Packs? {
        do{
            let realm = try Realm()
            if let entity = realm.objects(PacksEntity.self).filter("title == %@", title).first{
                let model = entity.packsModel()
                return model
            }
        }
        catch {
            return nil
        }
        return nil
    }
    
    func create(a: Packs) -> Bool {
        do{
            let realm = try Realm()
            let entity = PacksEntity(packs: a)
            try realm.write{
                realm.add(entity,update: true)
            }
        }
        catch{
            return false
        }
        return true
    }
    
    func delete(a: Packs) -> Bool {
        do{
            let realm = try Realm()
            try realm.write {
                let entityToDelete = realm.objects(PacksEntity.self).filter("id == %@", a.id)
                realm.delete(entityToDelete)
            }
        }
        catch{
            return false
        }
        return true
    }
    
    func update(a: Packs) -> Bool {
        return create(a:a)
    }
}


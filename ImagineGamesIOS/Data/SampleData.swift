//
//  SampleData.swift
//  ImagineGamesIOS
//
//  Created by Alberto Gurpegui Ramon on 11/02/2019.
//  Copyright © 2019 ImagineGames. All rights reserved.
//

import Foundation

func putAllData(){
    
    let repositoryFilter = LocalFilterRepository()
    let repositoryCategory = LocalSubCategoryRepository()
    let repositoryTableGames = LocalTableGamesRepository()
    let repositoryVideoGames = LocalVideoGamesRepository()
    let repositoryPack = LocalPacksRepository()
    
    let edad1 = Filter(id: "1", image: "menosde12", title: "<12")
    let edad2 = Filter(id: "2", image: "masde12", title: "12+")
    let edad3 = Filter(id: "3", image: "masde16", title: "16+")
    let edad4 = Filter(id: "4", image: "masde18", title: "18+")
    
    let jugadores1 = Filter(id: "5", image: "1jugador", title: "1")
    let jugadores2 = Filter(id: "6", image: "masde2jugadores", title: "2+")
    let jugadores3 = Filter(id: "7", image: "masde3jugadores", title: "3+")
    let jugadores4 = Filter(id: "8", image: "masde4jugadores", title: "4+")
    
    let precio1 = Filter(id: "9", image: "menosde20euros", title: "<20")
    let precio2 = Filter(id: "10", image: "entre20y40euros", title: "20-40")
    let precio3 = Filter(id: "11", image: "entre40y60euros", title: "40-60")
    let precio4 = Filter(id: "12", image: "masde60euros", title: "60+")
    
    let creacion1 = Filter(id: "13", image: "antesde1950", title: "<1950")
    let creacion2 = Filter(id: "14", image: "entre1950yel2000", title: "1950-2000")
    let creacion3 = Filter(id: "15", image: "entre2000yel2010", title: "2000-2010")
    let creacion4 = Filter(id: "16", image: "2010aActualidad", title: "2010-actualidad")
    
    
    
    let cluedo = TableGames(id: "1", image: "https://i.ytimg.com/vi/mNUZdxhOWGg/hqdefault.jpg", title: "Cluedo", body: "(Clue en América) es un juego de mesa de detectives y misterio originalmente publicado por Waddington Games (Reino Unido) en 1948. Fue desarrollado por Anthony Pratt, un empleado de un abogado de Birmingham, Inglaterra. Actualmente, se comercializa por la compañía de juguetes y juegos estadounidense Hasbro, que adquirió la compañía de juegos de mesa Parker Brothers, la cual lo comercializaba originariamente. El objetivo del juego es descubrir quién asesinó al Dr. Black, en la versión norteamericana (Dr. Negro en español, llamado Mr. Boddy, Sr. Cadavery en español), con qué arma, y en qué habitación se cometió el crimen.", price: "17.99", numberItems: 0, filters: [edad1,jugadores3,precio1,creacion1])
    
    let risk = TableGames(id: "2", image: "https://cdn.juguetilandia.com/images/articulos/11249g00.jpg", title: "Risk", body: "Risk (en inglés, riesgo) es un juego de mesa de carácter estratégico, creado por Albert Lamorisse en 1950 y comercializado desde 1958 por la empresa Parker Brothers (actualmente, parte de Hasbro). Este juego, basado en turnos, pertenece a la categoría de los juegos de guerra, al evocar las Guerras Napoleónicas, siendo su principal característica su simplicidad y abstracción al no pretender simular correctamente la estrategia militar en los territorios específicos, la geografía del mundo y la logística de las campañas extensas.", price: "34.24", numberItems: 0, filters: [edad2,jugadores2,precio2,creacion2])
    let lobo = TableGames(id: "3", image: "https://juegosdelamesaredonda.com/14302-thickbox_default/best-of-los-hombres-lobo-de-castronegro.jpg", title: "Lobo", body: "En general, existen dos bandos en la partida: Aldeanos y Hombres Lobo. El objetivo de cada equipo es eliminar a todos los miembros del otro, con la particularidad de que los aldeanos (la mayoría) ignoran quiénes son los hombres lobo (a excepción de ciertos aldeanos especiales, que pueden poseer información adicional), mientras que los hombres lobo saben quiénes son los hombres lobo y quiénes los aldeanos, pero están en minoría y deben por tanto fingir que son aldeanos. Es posible incluso añadir terceros bandos, con la inclusión del personaje de Cupido (si éste enamora a un aldeano y un hombre lobo) u otros personajes pertenecientes a ampliaciones, tales como el Lobo Albino o el Flautista. A todas las personas que reciban una carta se las denomina 'jugadores' y se considera que forman 'la aldea'.", price: "9.99", numberItems: 0, filters: [edad1,jugadores4,precio1,creacion3])
    let dragonesYmazmorras = TableGames(id: "4", image: "https://img.milanuncios.com/fg/2661/99/266199249_1.jpg?VersionId=7Hlvk2elcFC4zrSw8WU6kk2DVsiereGm", title: "Dragones y Mazmorras", body: "A diferencia de los juegos de miniaturas tradicionales, a cada jugador de Dragones y Mazmorras se le asigna solo un personaje con quien jugar de una formación militar. Estos personajes viven aventuras imaginarias en una ambientación fantástica. Dungeon Master (DM), que será uno de los jugadores, tendrá que cumplir la función de árbitro y de narrador, preparar y mantiener los escenarios en los que ocurren las aventuras y controlar a los personajes no jugadores que habitan. Los personajes se reúnen en grupos para interaccionar con estos habitantes y entre ellos mismos. Juntos resuelven conflictos, combaten con enemigos, encuentran tesoros y adquieren conocimientos y habilidades. En el proceso, los personajes obtienen puntos de experiencia con el fin de conseguri más poder a lo largo de varias partidas.", price: "49.99", numberItems: 0, filters:[edad2,jugadores2,precio3,creacion2])
    let polisYcacos = TableGames(id: "5", image: "https://images-na.ssl-images-amazon.com/images/I/81CDBuq-nRL._SX355_.jpg", title: "Polis y Cacos", body: "La finalidad del juego recae en los dos personajes principales: El policía gana si consigue capturar al ladrón, éste tiene que evita ser capturado por la policía y arriesgarse a salvar a sus aliados (ladrones).", price: "4.99", numberItems: 0, filters: [edad1,jugadores4,precio1,creacion2])
    let siSennorOscuro = TableGames(id: "6", image: "https://zacatrus.es//media/catalog/product/cache/1/image/560x/9df78eab33525d08d6e5fb8d27136e95/s/i/si-se_or-oscuro-caja.jpg", title: "Si Señor Oscuro", body: "¡Sí, Señor Oscuro! es un juego desenfadado de trasfondo fantástico y tono humorístico. Para jugar, sólo necesitas algo de capacidad de improvisación, y algunos amigos dispuestos a pasar un buen rato. El juego es simple de aprender y fácil de jugar.", price: "7.50", numberItems: 0, filters: [edad1,jugadores4,precio1,creacion4])
    let pathFinder = TableGames(id: "7", image: "https://zacatrus.es//media/catalog/product/cache/1/image/560x/9df78eab33525d08d6e5fb8d27136e95/p/a/pathfinder_juego_de_cartas.jpg", title: "Path Finder", body: "¡El juego de rol de Pathfinder te pone en el papel de un valeroso aventurero que trata de sobrevivir en un mundo plagado de magia y maldad! Pathfinder es una evolución de las reglas 3.5 del más famoso juego de rol de la historia, actualizadas a partir de las aportaciones de más de 50.000 jugadores.", price: "34.99", numberItems: 0, filters: [edad2,jugadores4,precio2,creacion3])
    
    
    
    let monopoly = TableGames(id: "8", image: "https://http2.mlstatic.com/monopoly-parker-brothers-nuevo-en-caja-el-original-D_NQ_NP_815705-MLV25069754915_092016-F.jpg", title: "Monopoly", body: "Monopoly es un juego de mesa basado en el intercambio y la compraventa de bienes raíces (normalmente, inspirados en los nombres de las calles de una determinada ciudad), hoy en día producido por la empresa estadounidense Hasbro de Rhode Island. Monopoly es uno de los juegos de mesa comerciales más vendidos del mundo.", price: "29.99", numberItems: 0, filters: [edad1,jugadores2,precio2,creacion1])
    
    let aventurerosAlTren = TableGames(id: "9", image: "https://tablerum.com/sites/tablerumcom/files/styles/people_346x346/public/aventureros-al-tren-america.jpg?itok=-3R9VOj5", title: "Aventureros al Tren", body: "Es un juego de mesa basado en el intercambio y la compraventa de bienes raíces (normalmente, inspirados en los nombres de las calles de una determinada ciudad), hoy en día producido por la empresa estadounidense Hasbro de Rhode Island.", price: "50.00", numberItems: 0, filters: [edad1,jugadores2,precio3,creacion1])
    
    let domino = TableGames(id: "10", image: "https://d2rovik8lnwcqe.cloudfront.net/media/2/photos/products/014484/14484-244-caja-domino-xl-17-1067x800_1_g.jpg", title: "Dominó", body: "El dominó es un juego de mesa que puede considerarse como una extensión de los dados. Aunque su origen se supone oriental y antiquísimo no parece que la forma actual fuese conocida en Europa hasta mediados del siglo XVIII, cuando lo introdujeron los italianos.", price: "10.00", numberItems: 0, filters: [edad1,jugadores2,precio1,creacion1])
    
    let ajedrez = TableGames(id: "11", image: "https://i.ytimg.com/vi/ZzrB16_wUa4/maxresdefault.jpg", title: "Ajedrez", body: "El ajedrez es un juego por turnos para 2 jugadores que se desarrolla sobre un tablero cuadriculado de 8 por 8 casillas, también llamadas escaques. En total hay 64 escaques que se alternan entre claros y oscuros.", price: "34.99", numberItems: 0, filters: [edad1,jugadores2,precio2,creacion1])
    
    let damas = TableGames(id: "12", image: "https://www.mar-plast.com/23098-large_default/juego-de-damas-caja-roja-cod-10.jpg", title: "Damas", body: "El juego de las damas consta de 24 peones divididos en 12 blancos y 12 negros y un tablero de 64 casillas (8×8) coloreadas alternativamente blancas y negras. La finalidad del juego es la captura o bloqueo de todas las piezas contrarias, de forma que no les sea posible realizar movimiento.", price: "19.99", numberItems: 0, filters: [edad1,jugadores2,precio1,creacion1])
    
    let hundirLaFlota = TableGames(id: "13", image: "https://deploreibol.files.wordpress.com/2011/09/hundir-la-flota.jpg", title: "Hundir la Flota", body: "El juego consiste en hundir la flota del contrincante. Para ello, debe colocar su propia flota de forma estratégica y encontrar y hundir con los disparos la flota contraria.", price: "15.00", numberItems: 0, filters: [edad1,jugadores2,precio1,creacion1])
    
    let ageOfEmpires = TableGames(id: "14", image: "https://www.justforgeeks.com.ar/wp-content/uploads/2014/10/17.jpg", title: "Age of Empires", body: "Juego de mesa por turnos en el que cada jugador elige una raza de entre las que hay, como, por ejemplo, egipcios, romanos, griegos... Debes dominar el tablero derrotando al ejercito de tu rival, mediante tiradas de dados exitosas, cartas con eventos especiales, y sobretodo, mucha estrategia.", price: "65.00", numberItems: 0, filters: [edad2,jugadores2,precio4,creacion3])
    
    
    
    let trivial = TableGames(id: "15", image: "https://zacatrus.es//media/catalog/product/t/r/trivial_puirsuit_clasico_1.jpg", title: "Trivial", body: "El Trivial Pursuit es un juego de mesa de Scott Abbott, un editor deportivo del diario Canadian Press, y Chris Haney, fotógrafo de la revistaMontreal Gazette. Desarrollaron la idea en diciembre de 1979 y su juego fue lanzado al mercado dos años después.", price: "45.00", numberItems: 0, filters: [edad4,jugadores2,precio3,creacion2])
    
    let cifrasYletras = TableGames(id: "16", image: "https://cloud10.todocoleccion.online/juegos-mesa/tc/2017/09/22/21/98588087.jpg", title: "Cifras y Letras", body: "El juego consta de dos tipos de pruebas. En las pruebas numéricas, aparecen 6 números escogidos aleatoriamente con los que deberás realizar operaciones simples (suma, resta, multiplicación y división) hasta conseguir el número que se plantea como objetivo.", price: "22.50", numberItems: 0, filters: [edad1,jugadores2,precio2,creacion4])
    
    let scrabble = TableGames(id: "17", image: "https://images-na.ssl-images-amazon.com/images/I/71hGAbOvAtL._SX355_.jpg", title: "Scrabble", body: "Scrabble es un juego de mesa en el cual cada jugador intenta ganar más puntos mediante la construcción de palabras sobre un tablero de 15x15 casillas. Las palabras pueden formarse horizontal o verticalmente y se pueden cruzar siempre y cuando aparezcan en el diccionario estándar.", price: "25.00", numberItems: 0, filters: [edad2,jugadores2,precio2,creacion4])
    
    let pasapalabra = TableGames(id: "18", image: "https://cloud10.todocoleccion.online/juegos-mesa/tc/2017/01/12/23/72171395.jpg", title: "Pasapalabra", body: "Se trata de la versión del juego de la ruleta de letras orientado a los jóvenes. El objetivo es encontrar la palabra cuya inicial aparece en la ruleta donde el tiempo es la clave. Jugar y mantener la mente despierta mientras nos desafían 5.500 preguntas.", price: "22.50", numberItems: 0, filters: [edad1,jugadores2,precio2,creacion4])
    
    let quienSoy = TableGames(id: "19", image: "https://http2.mlstatic.com/adivina-quien-de-hasbro-juego-de-mesa-D_NQ_NP_703985-MLA26027659591_092017-F.jpg", title: "¿Quien Soy?", body: "Juego de mesa muy divertido en el que tendrás que adivinar que personaje tienes en la frente con preguntas que solo pueden ser respondidas con si o no. Diversión y risas aseguradas.", price: "10.00", numberItems: 0, filters: [edad1,jugadores4,precio1,creacion3])
    
    let rummikub = TableGames(id: "20", image: "https://cloud10.todocoleccion.online/juegos-mesa/tc/2014/12/01/11/46520480.jpg", title: "Rummikub", body: "Para poder bajar fichas a la mesa los jugadores deben tener una o más líneas (combinaciones) que sumen 30 puntos. Los puntos son los que indica el número de la ficha, siendo el valor del comodín el de la casilla que ocupe esta. En caso de no tener combinaciones por un valor mínimo de 30 puntos, el jugador debe robar una ficha y pasar el turno al siguiente jugador.", price: "23.50", numberItems: 0, filters: [edad1,jugadores4,precio2,creacion1])
    
    
    
    let tresD = TableGames(id: "21", image: "https://cloud10.todocoleccion.online/maquetas/tc/2012/12/24/34887622.jpg", title: "3D", body: "Divertido y fácil de poner juntos: fácil de montar y desmontar, no requiere herramientas, diseñado para jugar varias veces.", price: "25.00", numberItems: 0, filters: [edad1,jugadores1,precio2,creacion2])
    
    let encajar = TableGames(id: "22", image: "https://tintoneti.es/wp-content/uploads/2018/07/30009014_02.jpg", title: "Encajar", body: "Puzle para encajar las formas de las fichas en el tablero, los más pequeños de la casa se lo pasarán en grande.", price: "9.99", numberItems: 0, filters: [edad1,jugadores1,precio1,creacion2])
    
    let bolaDelMundo = TableGames(id: "23", image: "https://images-na.ssl-images-amazon.com/images/I/81TLUWQeB3L._SY450_.jpg", title: "Bola del Mundo", body: "Descubre países, continentes, ríos, mares… Mientras construyes este maravillo puzle, aparte de ser educativo es súper divertido.", price: "28.50", numberItems: 0, filters: [edad1,jugadores1,precio2,creacion2])
    
    let arcoiris = TableGames(id: "24", image: "https://images-na.ssl-images-amazon.com/images/I/61O3-ox5xxL._SX425_.jpg", title: "Arcoiris", body: "Aprende de que colores está compuesto el arcoíris con este maravilloso puzle, los más pequeños se entretendrán encajando las piezas en orden para formar el arcoíris.", price: "10.75", numberItems: 0, filters: [edad1,jugadores1,precio1,creacion2])
    
    let cubos = TableGames(id: "25", image: "https://jugueteriaelpatiodemicasa.es/22248-large_default/puzzle-12-cubos-animales-de-colores.jpg", title: "Cubos", body: "Cada cara del cubo tiene una imagen, debes colocarlas de tal manera que con todos los cubos formes una imagen final con todos los cubos juntos.", price: "15.00", numberItems: 0, filters: [edad1,jugadores1,precio1,creacion2])
    
    let rubik = TableGames(id: "26", image: "https://images-na.ssl-images-amazon.com/images/I/61HI-nT1W4L._SY355_.jpg", title: "Rubik", body: "El famoso rompe cabezas, 9 caras de diferente color cada una, ordénalas para obtener todas las caras con su respectivo color.", price: "12.50", numberItems: 0, filters: [edad1,jugadores1,precio1,creacion2])
    
    let magnetico = TableGames(id: "27", image: "https://www.eurekakids.net/g/08505527/puzzle-magnetico-de-espana.jpg", title: "Magnético", body: "Haz infinidad de formas con los imanes de este puzzle, desde un cubo a un dinosaurio, lo que quieras, solo usa la imaginación.", price: "18.00", numberItems: 0, filters: [edad1,jugadores1,precio1,creacion3])
    
    
    
    let rolTG = SubCategory(id: "5", image: "rolTG", title: "ROL", tablegames: [cluedo,risk,lobo,dragonesYmazmorras,polisYcacos,siSennorOscuro,pathFinder], videogames: [])
    
    let estrategiaTG = SubCategory(id: "6", image: "estrategiaTG", title: "ESTRATEGIA TG", tablegames: [monopoly,aventurerosAlTren,domino,ajedrez,damas,hundirLaFlota,ageOfEmpires], videogames: [])
    
    let educativoTG = SubCategory(id: "7", image: "educativoTG", title: "EDUCATIVO", tablegames: [trivial,cifrasYletras,scrabble,pasapalabra,quienSoy,rummikub], videogames: [])
    
    let puzzleTG = SubCategory(id: "8", image: "puzzleTG", title: "PUZZLE", tablegames: [tresD,encajar,bolaDelMundo,arcoiris,cubos,rubik,magnetico], videogames: [])
    
    
    
    let wiiSportsResort = VideoGames(id: "1", image: "https://c1-ebgames.eb-cdn.com.au/merchandising/images/packshots/73278a4c3e2145e1b56da1c4e0a52550_Large.png", title: "Wii Sports Resort", body: "Wii Sports Resort es una colección de doce juegos deportivos, que incluye juegos mejorados de Wii Sports, junto con otros completamente nuevos, diseñados para utilizar la detección de movimiento que ofrece Wii Mote y el Nunchuk.", price: "22.50", numberItems: 0, filters: [edad1,jugadores2,precio2,creacion3])
    
    let justDance = VideoGames(id: "2", image: "https://i11a.3djuegos.com/juegos/15976/just_dance_2019/fotos/ficha/just_dance_2019-4580444.jpg", title: "Just Dance", body: "Es un juego de baile hecho para todos los públicos, en el cual no es necesario ningún tipo de accesorio y cualquiera puede manejar el juego gracias a los controles instintivos del mando de Wii.", price: "70.00", numberItems: 0, filters: [edad1,jugadores2,precio4,creacion3])
    
    let nba2k19 = VideoGames(id: "3", image: "https://http2.mlstatic.com/nba-2k19-pc-100-original-steam-D_NQ_NP_843693-MLA28517479087_102018-F.jpg", title: "NBA2K19", body: "NBA 2K19 es un videojuego de la legendaria saga de basket NBA desarrollada por 2KSports que, entre sus novedades, incluye 25 equipos de la Euroliga. Si te gustan los simuladores de baloncesto, NBA 2K no tiene rival.", price: "69.99", numberItems: 0, filters: [edad1,jugadores2,precio4,creacion4])
    
    let rocketLeague = VideoGames(id: "4", image: "https://static.wixstatic.com/media/9a7f4b_26e203eec5db45dfa17d396ecda1bf01~mv2.jpg", title: "Rocket League", body: "El juego se asemeja al fútbol, pero utilizando vehículos en lugar de jugadores y una pelota de gran tamaño. En los partidos pueden participar desde 1 vs 1 hasta 4 vs 4 jugadores distribuidos en dos equipos, naranja y azul.", price: "25.00", numberItems: 0, filters: [edad1,jugadores1,precio2,creacion4])
    
    let marioSportsSuperstars = VideoGames(id: "5", image: "https://cdn02.nintendo-europe.com/media/images/10_share_images/games_15/nintendo_3ds_25/H2x1_3DS_MarioSportsSuperstars_image1600w.jpg", title: "Mario Sports Superstars", body: "El videojuego consiste en cinco deportes: fútbol, béisbol, tenis, golf y carreras de caballos. A pesar de la cantidad de deportes contenidos, no son minijuegos, sino más bien, recreaciones a gran escala de cada deporte.", price: "27.50", numberItems: 0, filters: [edad1,jugadores2,precio2,creacion4])
    
    let marioTennis = VideoGames(id: "6", image: "https://cdn02.nintendo-europe.com/media/images/10_share_images/games_15/nintendo_3ds_25/SI_3DS_MarioTennisOpen_image1600w.jpg", title: "Mario Tennis", body: "En él, todos los personajes juegan tenis. En este videojuego aparece por primera vez Waluigi. Es el segundo videojuego de la saga.", price: "20.00", numberItems: 0, filters: [edad1,jugadores1,precio2,creacion3])
    
    
    
    let battlefield = VideoGames(id: "7", image: "https://i2.wp.com/culturageek.com.ar/wp-content/uploads/2018/11/Culturageek.com_.ar-Battlefield-5.jpg?fit=1200%2C800", title: "Battlefield V", body: "Los juegos se centran en el combate por equipos luchando desde mapas chicos a enormes, haciendo énfasis en el trabajo en equipo, tanto de infantería como de vehículos de tierra, aire y agua.", price: "70.00", numberItems: 0, filters: [edad4,jugadores4,precio4,creacion4])
    
    let callOfDuty = VideoGames(id: "8", image: "https://images-eds-ssl.xboxlive.com/image?url=8Oaj9Ryq1G1_p3lLnXlsaZgGzAie6Mnu24_PawYuDYIoH77pJ.X5Z.MqQPibUVTcYb1HkJjg7GMHqu224j3c9wXKUUFepgIriZ7wmoij1ketHHCQOLcv0kCdf7THAY5OA1ES8EGRsL91qRW_mhj6rXF9jc1Lt9Qon5gm.DTIRza5K.fqDt0C9feUR_tdAcogLjlzTzwxf53slhavFpes1LCSkA7cXRyvBINrjqnCy98-&h=1080&w=1920&format=jpg", title: "Call of Duty Black Ops 4", body: "La serie inicialmente se ambientaba en la Segunda Guerra Mundial, relatando personajes y combates acaecidos durante dicho conflicto bélico. Esto fue cambiando hasta la actualidad, donde los argumentos suceden en ambientes contemporáneos y ficticios.", price: "70.00", numberItems: 0, filters: [edad4,jugadores4,precio4,creacion4])
    
    let halo = VideoGames(id: "9", image: "https://content.halocdn.com/media/Default/games/halo-5-guardians/page/h5-guardians-facebook-1200x630-ba103624b3f34af79fe8cb2d340dce3f.jpg", title: "Halo", body: "La serie se centra en una guerra interestelar entre la humanidad y una alianza teocrática de alienígenas conocidos como Covenant o Pacto, y más tarde, se encontrarán más amenazas como los Floods y los Prometeos.", price: "58.75", numberItems: 0, filters: [edad3,jugadores4,precio4,creacion3])
    
    let medalOfHonor = VideoGames(id: "10", image: "https://static.posters.cz/image/1300/posters/medal-of-honor-calm-i8636.jpg", title: "Medal of Honor", body: "Es una serie de videojuegos creada y producida por Steven Spielberg. La temática de la serie siempre ha estado basada en los combates de la Segunda Guerra Mundial, tema de culto para Spielberg.", price: "58.00", numberItems: 0, filters: [edad4,jugadores4,precio3,creacion2])
    
    let civilWar = VideoGames(id: "11", image: "https://images-na.ssl-images-amazon.com/images/I/51b-25B2KjL.jpg", title: "Civil War", body: "Doce niveles basados en las más conocidos batallas de la Guerra Civil Estadounidense. Uno puede jugar como la Confederación o la Unión.", price: "38.00", numberItems: 0, filters: [edad3,jugadores1,precio2,creacion2])
    
    let killzone = VideoGames(id: "12", image: "https://mlstaticquic-a.akamaihd.net/juego-killzone-shadow-fall-nuevo-sin-uso-en-caja--D_NQ_NP_15063-MLU20095358060_052014-F.jpg", title: "Killzone", body: "Killzone se centra en el control del capitán Jan Templar miembro de ISA que con la ayuda de su equipo, tendrá como objetivo repeler la invasión de los Helghast y hacerlos retroceder a cualquier precio.", price: "27.00", numberItems: 0, filters: [edad3,jugadores1,precio2,creacion2])
    
    let gearsOfWar = VideoGames(id: "13", image: "https://images-na.ssl-images-amazon.com/images/I/91-lNUrBnSL._SX425_.jpg", title: "Gears of War", body: "En Gears of War, el protagonista es Marcus Fenix, un soldado o «Gear» en la Coalición de Gobiernos Ordenados, lleva a su equipo en una misión para instalar la bomba de masa ligera y destruir a la horda de Locust en sus fortalezas subterráneas.", price: "45.00", numberItems: 0, filters: [edad3,jugadores1,precio3,creacion3])
    
    
    
    let elSennorDeLosAnillos = VideoGames(id: "14", image: "https://media.redadn.es/imagenes/el-senor-de-los-anillos-la-batalla-por-la-tierra-media_168685.jpg", title: "El Señor de los Anillos", body: "La localización de las estructuras no es decidida por el jugador. Hay estructuras abandonadas por el mapa y los jugadores las toman para sí, y entonces generan una ciudadela y puntos donde colocar estructuras o torres defensivas.", price: "26.50", numberItems: 0, filters: [edad1,jugadores1,precio2,creacion3])
    
    let totalWar = VideoGames(id: "15", image: "https://vignette.wikia.nocookie.net/labibliotecadelviejomundo/images/2/27/Total_War_Warhammer_PC_caratula.jpg/revision/latest?cb=20160308172610&path-prefix=es", title: "Total War", body: "La dinámica de estos videojuegos consiste en combinar estrategia por turnos y tácticas de batalla en tiempo real, además de la animación de las batallas.", price: "28.00", numberItems: 0, filters: [edad1,jugadores1,precio2,creacion3])
    
    let startCraft = VideoGames(id: "16", image: "https://images-eu.ssl-images-amazon.com/images/I/51YFGXJVIdL.jpg", title: "Startcraft", body: "La serie se centra en la disputa galáctica por la dominación entre cuatro especies - la adaptable y móvil Terran, la insectoide Zerg, los enigmáticos Protoss y la raza creadora Xel'Naga - en una parte distante de la Vía Láctea denominada como el sector Koprulu durante el principio del siglo XXVI.", price: "70.00", numberItems: 0, filters: [edad3,jugadores1,precio4,creacion2])
    
    let rainbowSixSiege = VideoGames(id: "17", image: "https://steamuserimages-a.akamaihd.net/ugc/97225126782412379/F1AABD7758EF5C0403F4411DB31BB11C99650267/", title: "Rainbow Six Siege", body: "El juego consiste en partidas de equipos de cinco atacantes contra cinco defensores. Los defensores deben defender un objetivo dentro de una estructura, ya sea un rehén, dos bombas o un contenedor biológico.", price: "75.00", numberItems: 0, filters: [edad4,jugadores1,precio4,creacion4])
    
    let imperium = VideoGames(id: "18", image: "https://cloud10.todocoleccion.online/videojuegos-pc/tc/2015/06/21/19/49966782.jpg", title: "Imperium", body: "Control total del mundo, elige tu raza entre todas las que el juego te ofrece y lucha por llevarla a la cima, un juego en el que deberás poner tus cinco sentidos y tu mente en la estrategia y coronarte rey del mundo.", price: "29.99", numberItems: 0, filters: [edad4,jugadores1,precio2,creacion3])
    
    let hearthStone = VideoGames(id: "19", image: "https://d2q63o9r0h0ohi.cloudfront.net/images/fb-share/facebook-share-default-04812acb25dba13239f3dbe52750ff0f4ae58cc52b5924e7cf6ffea5e1b8993d4f07bb5918b25cb9f8bb8f626f694e20e579f8eb50a43de1c1fd2fc1d6c81a60.jpg", title: "Hearthstone", body: "Hearthstone es un juego de cartas coleccionables en línea que se basa en partidas por turnos entre dos oponentes, operado a través del Battle.net de Blizzard. Los jugadores pueden escoger entre diferentes modos de juego que ofrecen diferentes experiencias.", price: "5.50", numberItems: 0, filters: [edad1,jugadores1,precio1,creacion4])
    
    let angryBirds = VideoGames(id: "20", image: "https://articles-images.sftcdn.net/wp-content/uploads/sites/2/2014/11/Angry-Birds-New-Levels-and-Power-Ups-Trailer_1.jpg", title: "Angrybirds", body: "Derrota a los cerdos que quieren robar tus huevos, lánzate, explota, conviértete en 3… Haz todo lo posible por destruir la base de los cerdos y recuperar tus huevos de mano de esos sucios puercos.", price: "2.50", numberItems: 0, filters: [edad1,jugadores1,precio1,creacion3])
    
    
    
    let marioBros = VideoGames(id: "21", image: "https://www.electronicthings.com.ar/blog/wp-content/uploads/NewSuperMarioBros.jpg", title: "Mario Bros", body: "La princesa ha sido secuestrada, el reino champiñón está desolado, y solo un fontanero podrá rescatarla del castillo en donde está secuestrada por manos del temible monstruo llamado Bowser.", price: "25.00", numberItems: 0, filters: [edad1,jugadores1,precio2,creacion2])
    
    let pokemon = VideoGames(id: "22", image: "https://www.alfabetajuega.com/wp-content/uploads/2018/10/162869.alfabetajuega-pokemon-juegos-150916.jpg", title: "Pokemon", body: "Un juego donde podrás capturar, coleccionar y cuidar una infinidad de criaturas poderosas llamadas Pokémon. Deberás ser el campeón de la liga Pokémon con ayuda de tus Pokémon. ¡Conviértete en un héroe!.", price: "30.00", numberItems: 0, filters: [edad1,jugadores1,precio3,creacion2])
    
    let worldOfWarcraft = VideoGames(id: "23", image: "https://blznav.akamaized.net/img/games/cards/card-world-of-warcraft-54576e6364584e35.jpg", title: "World Of Warcraft", body: "La Alianza y la Nueva Horda viven en un estado de guerra fría, y mantienen una frágil y quebradiza paz, mientras reconstruyen sus reinos y tratan de recuperar su prosperidad. En Ventormenta, el rey Varian Wrynn ha desaparecido en circunstancias misteriosas mientras viajaba en misión diplomática a la isla de Theramore…", price: "65.00", numberItems: 0, filters: [edad3,jugadores4,precio4,creacion3])
    
    let horizon = VideoGames(id: "24", image: "https://http2.mlstatic.com/horizon-zero-dawn-ps4-fisico-nuevo-sellado-caja-plastica-D_NQ_NP_720332-MLA27257398526_042018-F.jpg", title: "Horizon", body: "Se remonta aproximadamente 1000 años en el futuro, en un escenario postapocalíptico donde los seres humanos han vuelto a la época de las sociedades tribales primitivas como resultado de una catástrofe de origen desconocida.", price: "50.00", numberItems: 0, filters: [edad3,jugadores1,precio3,creacion4])
    
    let theLastOfUs = VideoGames(id: "25", image: "https://lh3.googleusercontent.com/zOphivZUBIZUW8Iuz7QETANL1qZ0Bmi2d9cV1cF2R2wwXSm7Zl_H1i_V4hoOMgcngg0T34tK=w640-h400-e365", title: "The Last of Us", body: "Se desata una pandemia en Estados Unidos ocasionada por una cepa del hongo Cordyceps, que al infectar a los humanos los convierte en criaturas caníbales, y que se puede transmitir a través de una simple mordida…", price: "45.50", numberItems: 0, filters: [edad4,jugadores1,precio3,creacion4])
    
    let monsterHunter = VideoGames(id: "26", image: "https://www.nvidia.com/content/dam/en-zz/Solutions/geforce/news/articles/monster-hunter-world-geforce-gtx-bundle-ogimage.jpg", title: "Monster Hunter", body: "Los cazadores en el mundo de Monster Hunter son parte de un gremio de cazadores que se encarga de los problemas ocasionados por los monstruos en toda la región. El personaje llega a una de las numerosas aldeas de la región esperando convertirse en el héroe del lugar mientras va completando las diferentes misiones que le asignan.", price: "49.99", numberItems: 0, filters: [edad3,jugadores1,precio3,creacion4])
    
    let godOfWar = VideoGames(id: "27", image: "https://www.ultimagame.es/god-war-ascension/imagen-i7638-pge.jpg", title: "God of War", body: "La infancia de Kratos es explicada durante el desarrollo del primer juego. Es maltratado, como solía hacerse a los niños altos y fuertes en la Antigua Grecia. A su hermano, al tener una marca que el oráculo predijo la portaría el que causara la perdición del Olimpo, Ares lo secuestra y lo lleva al reino de la muerte.", price: "50.00", numberItems: 0, filters: [edad4,jugadores1,precio3,creacion3])
    
    
    
    let deporteVG = SubCategory(id: "1", image: "deporte", title: "DEPORTE", tablegames: [], videogames: [wiiSportsResort,justDance,nba2k19,rocketLeague,marioSportsSuperstars,marioTennis])
    
    let guerraVG = SubCategory(id: "2", image: "guerra", title: "GUERRA", tablegames: [], videogames: [battlefield,callOfDuty,halo,medalOfHonor,civilWar,killzone,gearsOfWar])
    
    let estrategiaVG = SubCategory(id: "3", image: "estrategia_videojuegos", title: "ESTRATEGIA VG", tablegames: [], videogames: [elSennorDeLosAnillos,totalWar,startCraft,rainbowSixSiege,imperium,hearthStone,angryBirds])
    
    let aventuraVG = SubCategory(id: "4", image: "aventura", title: "AVENTURAS", tablegames: [], videogames: [marioBros,pokemon,worldOfWarcraft,horizon,theLastOfUs,monsterHunter,godOfWar])
    
    
    
    let pack1 = Packs(id: "1", image: "pack_aleatorio", title: "Pack Aleatorio", descriptions: "juegos completamente aleatorios de entre todos los juegos disponibles, tanto videojuegos como juegos de mesa", price: "70", count: 1, tablegamesPacks: [], videogamesPacks: [])
    let pack2 = Packs(id: "2", image: "pack_aventura", title: "Pack Aventura", descriptions: "Pokémon rojo" + "\n" + "-World of Warcraft" + "\n" + "-Super Mario bross 3" + "\n" + "-Dragones y mazmorras", price: "84", count: 1, tablegamesPacks: [], videogamesPacks: [])
    let pack3 = Packs(id: "3", image: "pack_familiar", title: "Pack Familiar", descriptions: "-Monopoly" + "\n" + "-Pictionary" + "\n" + "-Wii Sports" + "\n" + "-Mario Party", price: "72", count: 1, tablegamesPacks: [], videogamesPacks: [])
    let pack4 = Packs(id: "4", image: "pack_guerra", title: "Pack Guerra", descriptions: "-Call of duty Black Ops III" + "\n" + "-Battlefield V" + "\n" + "-Hundir la flota" + "\n" + "-Halo V", price: "133", count: 1, tablegamesPacks: [], videogamesPacks: [])
    let pack5 = Packs(id: "5", image: "pack_navidad", title: "Pack Navidad", descriptions: "-Mario y Sonic en los juegos olimpicos de invierno" + "\n" + "-Wii Sports Resorts" + "\n" + "-Just Dance" + "\n" + "-Scrabble", price: "78", count: 1, tablegamesPacks: [], videogamesPacks: [])
    let pack6 = Packs(id: "6", image: "pack_san_valentin", title: "Pack San valentín", descriptions: "-Sims" + "\n" + "-Nintendogs" + "\n" + "-Farm Simulator" + "\n" + "-Project rub", price: "96", count: 1, tablegamesPacks: [], videogamesPacks: [])
    let pack7 = Packs(id: "7", image: "esenciales_de_mesa", title: "Esenciales de mesa", descriptions: "-Rummikub" + "\n" + "-Parchis" + "\n" + "-Dominó" + "\n" + "-Cluedo", price: "53", count: 1, tablegamesPacks: [], videogamesPacks: [])
    let pack8 = Packs(id: "8", image: "esenciales_de_videojuegos", title: "Esenciales de videojuegos", descriptions: "-The Legend of Zelda: Ocarina of Time" + "\n" + "-Pokémon Esmeralda" + "\n" + "-Call of Duty World at War" + "\n" + "-Battlefield Bad company", price: "120", count: 1, tablegamesPacks: [], videogamesPacks: [])
    
    
    
    repositoryFilter.create(a: edad1)
    repositoryFilter.create(a: edad2)
    repositoryFilter.create(a: edad3)
    repositoryFilter.create(a: edad4)
    repositoryFilter.create(a: jugadores1)
    repositoryFilter.create(a: jugadores2)
    repositoryFilter.create(a: jugadores3)
    repositoryFilter.create(a: jugadores4)
    repositoryFilter.create(a: precio1)
    repositoryFilter.create(a: precio2)
    repositoryFilter.create(a: precio3)
    repositoryFilter.create(a: precio4)
    repositoryFilter.create(a: creacion1)
    repositoryFilter.create(a: creacion2)
    repositoryFilter.create(a: creacion3)
    repositoryFilter.create(a: creacion4)
    
    
    
    repositoryTableGames.create(a: cluedo)
    repositoryTableGames.create(a: risk)
    repositoryTableGames.create(a: lobo)
    repositoryTableGames.create(a: dragonesYmazmorras)
    repositoryTableGames.create(a: polisYcacos)
    repositoryTableGames.create(a: siSennorOscuro)
    repositoryTableGames.create(a: pathFinder)
    
    repositoryTableGames.create(a: monopoly)
    repositoryTableGames.create(a: aventurerosAlTren)
    repositoryTableGames.create(a: domino)
    repositoryTableGames.create(a: ajedrez)
    repositoryTableGames.create(a: damas)
    repositoryTableGames.create(a: hundirLaFlota)
    repositoryTableGames.create(a: ageOfEmpires)
    
    repositoryTableGames.create(a: trivial)
    repositoryTableGames.create(a: cifrasYletras)
    repositoryTableGames.create(a: scrabble)
    repositoryTableGames.create(a: pasapalabra)
    repositoryTableGames.create(a: quienSoy)
    repositoryTableGames.create(a: rummikub)
    
    repositoryTableGames.create(a: tresD)
    repositoryTableGames.create(a: encajar)
    repositoryTableGames.create(a: bolaDelMundo)
    repositoryTableGames.create(a: arcoiris)
    repositoryTableGames.create(a: cubos)
    repositoryTableGames.create(a: rubik)
    repositoryTableGames.create(a: magnetico)
    
    
    
    repositoryVideoGames.create(a: wiiSportsResort)
    repositoryVideoGames.create(a: justDance)
    repositoryVideoGames.create(a: nba2k19)
    repositoryVideoGames.create(a: rocketLeague)
    repositoryVideoGames.create(a: marioSportsSuperstars)
    repositoryVideoGames.create(a: marioTennis)
    
    repositoryVideoGames.create(a: battlefield)
    repositoryVideoGames.create(a: callOfDuty)
    repositoryVideoGames.create(a: halo)
    repositoryVideoGames.create(a: medalOfHonor)
    repositoryVideoGames.create(a: civilWar)
    repositoryVideoGames.create(a: killzone)
    repositoryVideoGames.create(a: gearsOfWar)
    
    repositoryVideoGames.create(a: elSennorDeLosAnillos)
    repositoryVideoGames.create(a: totalWar)
    repositoryVideoGames.create(a: startCraft)
    repositoryVideoGames.create(a: rainbowSixSiege)
    repositoryVideoGames.create(a: imperium)
    repositoryVideoGames.create(a: hearthStone)
    repositoryVideoGames.create(a: angryBirds)
    
    repositoryVideoGames.create(a: marioBros)
    repositoryVideoGames.create(a: pokemon)
    repositoryVideoGames.create(a: worldOfWarcraft)
    repositoryVideoGames.create(a: horizon)
    repositoryVideoGames.create(a: theLastOfUs)
    repositoryVideoGames.create(a: monsterHunter)
    repositoryVideoGames.create(a: godOfWar)
    
    
    
    repositoryCategory.create(a: rolTG)
    repositoryCategory.create(a: estrategiaTG)
    repositoryCategory.create(a: educativoTG)
    repositoryCategory.create(a: puzzleTG)
    
    
    
    repositoryCategory.create(a: deporteVG)
    repositoryCategory.create(a: guerraVG)
    repositoryCategory.create(a: estrategiaVG)
    repositoryCategory.create(a: aventuraVG)
    
    
    repositoryPack.create(a: pack1)
    repositoryPack.create(a: pack2)
    repositoryPack.create(a: pack3)
    repositoryPack.create(a: pack4)
    repositoryPack.create(a: pack5)
    repositoryPack.create(a: pack6)
    repositoryPack.create(a: pack7)
    repositoryPack.create(a: pack8)
}
